# megasync-nopdfium

Easy automated syncing between your computers and your MEGA cloud drive(stripped of pdfium dependency)

https://github.com/meganz/MEGAsync

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/cloud/megasync-nopdfium.git
```
<br><br>
PKGBUILD used for patch (initial by Rafael)

```
#!/hint/bash
# Maintainer : bartus <arch-local-repo(at).bartus.33mail.com>
# Contributor : Rafał Kozdrój <kozeid2+aur@gmail.com>
# Contributor : kikadf <kikadf.01@gmail.com>
# Contributor : Daniel Henry <d at hackr dot pl>
# Contributor : Miguel Revilla <yo at  miguelrevilla dot com>
# Contributor : Alfonso Saavedra "Son Link" <sonlink.dourden@gmail.com>
# Contributor : Hexchain Tong <i at hexchain dot org>
# shellcheck disable=SC2034,SC2154 # unused/uninitialized variables
# shellcheck disable=SC2164 # cd safe
# Modify: Rafael from RebornOS

pkgname=megasync-nopdfium
pkgver=4.5.3.0
pkgrel=1.1
pkgdesc="Easy automated syncing between your computers and your MEGA cloud drive(stripped of pdfium dependency)"
arch=('i686' 'x86_64')
provides=(megasync=$pkgver)
conflicts=(megasync)
url="https://github.com/meganz/MEGAsync"
license=('custom:MEGA LIMITED CODE REVIEW LICENCE')
depends=(c-ares crypto++ libsodium libuv libmediainfo libraw qt5-base qt5-svg qt5-x11extras ffmpeg)
makedepends=(qt5-tools swig doxygen lsb-release git)
_extname="_Win"
source=("git+https://github.com/meganz/MEGAsync.git#tag=v${pkgver}${_extname}"
        "meganz-sdk::git+https://github.com/meganz/sdk.git"
        "ffmpeg.patch")
sha256sums=('SKIP'
            'SKIP'
            '81dee8a4cf16ab92492799d5cd63272d43409f2b83b9d66768e56b0be9c39dd0')

prepare() {
    cd "MEGAsync"
    git config submodule.src/MEGASync/mega.url "../meganz-sdk"
    git submodule update
    
    cd "src/MEGASync/mega"
    patch -Np1 -i "$srcdir/ffmpeg.patch"
    cd ..
    sed -i '/DEFINES += REQUIRE_HAVE_PDFIUM/d' MEGASync.pro
    sed -i '/CONFIG += USE_PDFIUM/d' MEGASync.pro
}

build() {
    # build sdk
    cd "MEGAsync/src/MEGASync/mega"

    export PKG_CONFIG_PATH="/usr/lib/ffmpeg3.4/pkgconfig"
    export CFLAGS+=" -I/usr/include/ffmpeg3.4"
    export CXXFLAGS+=" -I/usr/include/ffmpeg3.4"
    export LDFLAGS+=" -L/usr/lib/ffmpeg3.4"

    ./autogen.sh
    ./configure \
        --disable-shared \
        --enable-static \
        --disable-silent-rules \
        --disable-curl-checks \
        --disable-megaapi \
        --with-ffmpeg \
        --with-cryptopp \
        --with-sodium \
        --with-zlib \
        --with-sqlite \
        --with-cares \
        --with-curl \
        --without-freeimage \
        --with-libuv \
        --disable-posix-threads \
        --disable-examples \
        --with-libzen \
        --with-libmediainfo \
        --prefix="${srcdir}/MEGAsync/src/MEGASync/mega/bindings/qt/3rdparty"

    # build megasync
    cd "../.."
    qmake-qt5 \
        "LIBS += -L/usr/lib/ffmpeg3.4" \
        "INCLUDEPATH += /usr/include/ffmpeg3.4" \
        "CONFIG += FULLREQUIREMENTS" \
        MEGA.pro
    lrelease-qt5 MEGASync/MEGASync.pro
    make
}

package () {
    cd "MEGAsync"
    install -Dm 644 LICENCE.md "${pkgdir}/usr/share/licenses/$pkgname/LICENCE"
    install -Dm 644 installer/terms.txt "${pkgdir}/usr/share/licenses/$pkgname/terms.txt"
    install -Dm 644 src/MEGASync/mega/LICENSE "${pkgdir}/usr/share/licenses/$pkgname/SDK-LICENCE"
    
    cd "src"
    install -dm 755 "${pkgdir}/usr/bin"
    make INSTALL_ROOT="${pkgdir}" TARGET="${pkgdir}/usr/bin/megasync" install

    install -Dm 755 "MEGASync/megasync" "${pkgdir}/usr/bin/megasync"
}
```

